/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heldrih.ivona.fon.fisweb.constants;

/**
 *
 * @author Heca
 */
public interface ActionConstants {

    public static final String URL_LOGIN = "/login";
    public static final String URL_LOGOUT = "/logout";
    public static final String URL_SAVE_DEPARTMENT = "/department/save";
    public static final String URL_ALL_DEPARTMENT = "/department/all";
    public static final String URL_UPDATE_DEPARTMENT = "/department/update";
    public static final String URL_DELETE_DEPARTMENT = "/department/delete";

}
