/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heldrih.ivona.fon.fisweb.listener;

import  heldrih.ivona.fon.fisweb.model.Department;
import  heldrih.ivona.fon.fisweb.model.User;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Web application lifecycle listener.
 *
 * @author Heca
 */
@WebListener
public class WebAppListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("users", createUsers());
        sce.getServletContext().setAttribute("departments", new ArrayList<Department>());

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

    private List<User> createUsers() {
        return new ArrayList<User>() {
            {
                add(new User("ivona", "heldrih", "ivonaheldrih@gmail.com", "ivona"));
                add(new User("teodora", "heldrih", "teodoraheldrih@gmail.com", "teodora"));
                add(new User("pera", "peric", "pera@gmail.com", "pera"));
            }
        };
    }

}
