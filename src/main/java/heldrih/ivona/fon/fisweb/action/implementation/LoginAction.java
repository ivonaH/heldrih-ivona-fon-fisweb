/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heldrih.ivona.fon.fisweb.action.implementation;

import heldrih.ivona.fon.fisweb.action.AbstractAction;
import heldrih.ivona.fon.fisweb.constants.PageConstants;
import heldrih.ivona.fon.fisweb.model.User;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Heca
 */
public class LoginAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        if (email.equals("") || password.equals("")) {
            request.setAttribute("message", "Enter username and password!");
            return PageConstants.VIEW_LOGIN;
        }
        User user = findUser(request, email, password);
        if (user == null) {
            request.setAttribute("message", "User does not exist!");
            return PageConstants.VIEW_LOGIN;
        } else {
            request.getSession(true).setAttribute("loginUser", user);
            return PageConstants.VIEW_HOME;
        }
    }

    private User findUser(HttpServletRequest request, String email, String password) {
        List<User> users = (List<User>) request.getServletContext().getAttribute("users");
        for (User user : users) {
            if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

}
