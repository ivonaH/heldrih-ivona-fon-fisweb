/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heldrih.ivona.fon.fisweb.action.implementation;

import heldrih.ivona.fon.fisweb.action.AbstractAction;
import heldrih.ivona.fon.fisweb.constants.PageConstants;
import heldrih.ivona.fon.fisweb.model.Department;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Heca
 */
public class DeleteDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {

        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("USAO u delete sa id:" + id);
        List<Department> departments = (List<Department>) request.getServletContext().getAttribute("departments");
        if (departments == null) {
            System.out.println("NULL JE");
        }
        for (Department department : departments) {
            if (department.getId() == id) {
                departments.remove(department);
                System.out.println("OBRISAO");
                break;
            }
        }
        request.setAttribute("message", "Department is deleted.");
        System.out.println("STAVIO ATRIBUT");
        return PageConstants.VIEW_DELETE_DEPARTMENT;
    }

}
