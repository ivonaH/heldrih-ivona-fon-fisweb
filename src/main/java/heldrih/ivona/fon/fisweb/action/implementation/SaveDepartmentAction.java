/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heldrih.ivona.fon.fisweb.action.implementation;

import heldrih.ivona.fon.fisweb.action.AbstractAction;
import heldrih.ivona.fon.fisweb.constants.PageConstants;
import heldrih.ivona.fon.fisweb.model.Department;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Heca
 */
public class SaveDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        String shortname = (String) request.getParameter("shortname");
        String name = (String) request.getParameter("name");
        Department department = new Department(shortname, name);
        ArrayList<Department> departments = (ArrayList<Department>) request.getServletContext().getAttribute("departments");
        System.out.println(name+" "+shortname);
        if (shortname == null || name == null || shortname=="" || name=="") {
            request.setAttribute("message", "Enter name and shortname! ");
        } else if (departments.contains(department)) {
            request.setAttribute("message", "Department already exist with that name!");
        } else {
            int id = generateId(departments);
            department.setId(id);
            departments.add(department);
            request.setAttribute("message", "Department is saved!");
        }

        return PageConstants.VIEW_SAVE_DEPARTMENT;
    }

    private int generateId(ArrayList<Department> departments) {
        int max = -1;
        for (Department department : departments) {
            if (max < department.getId()) {
                max = department.getId();
            }
        }
        return max+1;
    }

}
