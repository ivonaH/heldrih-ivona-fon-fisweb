/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heldrih.ivona.fon.fisweb.action.factory;

import heldrih.ivona.fon.fisweb.action.AbstractAction;
import heldrih.ivona.fon.fisweb.action.implementation.AllDepartmentsAction;
import heldrih.ivona.fon.fisweb.action.implementation.DeleteDepartmentAction;
import heldrih.ivona.fon.fisweb.action.implementation.LoginAction;
import heldrih.ivona.fon.fisweb.action.implementation.LogoutAction;
import heldrih.ivona.fon.fisweb.action.implementation.SaveDepartmentAction;
import heldrih.ivona.fon.fisweb.action.implementation.UpdateDepartmentAction;
import heldrih.ivona.fon.fisweb.constants.ActionConstants;

/**
 *
 * @author Heca
 */
public class ActionFactory {

    public static AbstractAction createActionFactory(String actionName) {
        AbstractAction action = null;
        if (actionName.equals(ActionConstants.URL_LOGIN)) {
            action = new LoginAction();
        }
        if (actionName.equals(ActionConstants.URL_ALL_DEPARTMENT)) {
            action = new AllDepartmentsAction();
        }
        if (actionName.equals(ActionConstants.URL_SAVE_DEPARTMENT)) {
            action = new SaveDepartmentAction();
        }
        if (actionName.startsWith(ActionConstants.URL_UPDATE_DEPARTMENT)) {
            action = new UpdateDepartmentAction();
        }
        if (actionName.startsWith(ActionConstants.URL_DELETE_DEPARTMENT)) {
            action = new DeleteDepartmentAction();
        }
        if (actionName.contains(ActionConstants.URL_LOGOUT)) {
            System.out.println("USAO U LOGOUT ACTION");
            action = new LogoutAction();
        }

        return action;
    }

}
