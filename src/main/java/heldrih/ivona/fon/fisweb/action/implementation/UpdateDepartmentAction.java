/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heldrih.ivona.fon.fisweb.action.implementation;

import heldrih.ivona.fon.fisweb.action.AbstractAction;
import heldrih.ivona.fon.fisweb.constants.PageConstants;
import heldrih.ivona.fon.fisweb.model.Department;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Heca
 */
public class UpdateDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String shortname = request.getParameter("shortname");

        List<Department> departments = (List<Department>) request.getServletContext().getAttribute("departments");
        for (Department department : departments) {
            if (department.getId() == id) {
                if (!department.getShortname().equals(shortname) && shortname != null && !shortname.equals("")) {
                    department.setShortname(shortname);
                    request.setAttribute("message", "Department updated.");
                }
                if (!department.getName().equals(name) && name != null && !name.equals("")) {
                    department.setName(name);
                    request.setAttribute("message", "Department updated.");
                }
                request.setAttribute("department", department);
                break;
            }
        }

        return PageConstants.VIEW_UPDATE_DEPARTMENT;
    }

}
