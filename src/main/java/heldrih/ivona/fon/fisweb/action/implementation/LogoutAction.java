/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heldrih.ivona.fon.fisweb.action.implementation;

import heldrih.ivona.fon.fisweb.action.AbstractAction;
import heldrih.ivona.fon.fisweb.constants.PageConstants;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Heca
 */
public class LogoutAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        request.setAttribute("message", "Logout successful");
        request.getSession().setAttribute("loginUser", null);
        return PageConstants.VIEW_LOGOUT;
    }

}
