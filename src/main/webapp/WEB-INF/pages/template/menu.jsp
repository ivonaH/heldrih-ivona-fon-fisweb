<%-- 
    Document   : template
    Created on : Apr 22, 2020, 1:54:13 PM
    Author     : Heca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark" >
            <a href="/ivona/application/department/save"> Add department</a>
            <a href="/ivona/application/department/all"> All departments</a>
            <a href="/ivona/application/logout" style="color:greenyellow;"><b>Logout</b></a>
        </nav>
        
        <div style="background-color:greenyellow; display:block">
            <b>  
                Current user: ${sessionScope.loginUser.ime} ${sessionScope.loginUser.prezime}
            </b>
        </div>
    </body>
</html>
