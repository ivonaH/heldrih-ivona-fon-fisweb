<%-- 
    Document   : add
    Created on : Apr 21, 2020, 9:35:16 PM
    Author     : Heca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Department</title>
    </head>
    <body>
        <%@include file="../template/menu.jsp" %>
        <div class="container">

            <div class="row">
                <div class="col">
                </div>
                <div class="col-6">
                    <h1>New Department:</h1>  
                    <div style="color:red">
                        ${message}
                    </div>
                    <form action="/ivona/application/department/save" method="POST">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="shortname">Shortname:</label>
                            <input type="text" class="form-control" name="shortname" id="shortname" placeholder="Enter shortname">
                        </div>
                        <input type="submit" value="Save"/>

                    </form>

                </div>
                <div class="col">
                </div>
            </div>
        </div>
    </body>
</html>
