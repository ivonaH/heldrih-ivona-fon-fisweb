<%-- 
    Document   : all
    Created on : Apr 21, 2020, 9:25:33 PM
    Author     : Heca
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>All Departments Page</title>
    </head>
    <body>
        <%@include file="../template/menu.jsp" %>
        <div class="container">

            <div class="row">
                <div class="col">
                </div>
                <div class="col-6">
                    <h1>All Departments:</h1>
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Shortname</th>
                                <th scope="col">Name</th>
                                <th scope="col">Delete</th>
                                <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="department" items="${applicationScope.departments}">
                                <tr>
                                    <td>${department.id}</td>
                                    <td>${department.shortname}</td>
                                    <td>${department.name}</td>
                                    <td>
                                        <a href="/ivona/application/department/delete?id=${department.id}">Delete</a>
                                    </td>
                                    <td>
                                        <a href="/ivona/application/department/update?id=${department.id}">Edit</a>
                                    </td>
                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>
                </div>
                <div class="col">
                </div>
            </div>
        </div>
    </body>
</html>
