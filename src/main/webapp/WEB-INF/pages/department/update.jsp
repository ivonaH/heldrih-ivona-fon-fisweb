<%-- 
    Document   : update
    Created on : Apr 22, 2020, 11:00:34 AM
    Author     : Heca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Department Page</title>
    </head>
    <body>
        <%@include file="../template/menu.jsp" %>
        <div class="container">

            <div class="row">
                <div class="col">
                </div>
                <div class="col-6">
                    <h1>Update Department Page</h1>
                    <div style="color:red">
                        ${message}
                    </div>
                    


                    <form action="/ivona/application/department/update" method="POST">
                        <table>
                            <tbody>
                                <tr>
                                    <td>ID:</td>
                                    <td><input type="text" name="id" value="${department.id}" readonly="readonly" /></td>
                                </tr>
                                <tr>
                                    <td>Name:</td>
                                    <td><input type="text" name="name" id="name" value="${department.name}"/></td>
                                </tr>
                                <tr>
                                    <td>Shortname:</td>
                                    <td><input type="text" name="shortname" id="shortname" value="${department.shortname}" /></td>
                                </tr>
                                <tr>
                                    <td><input type="submit" value="Update"/></td>
                                </tr>
                            </tbody>
                        </table>

                    </form> 
                </div>
                <div class="col">
                </div>
            </div>
        </div>

    </body>
</html>
