<%-- 
    Document   : department
    Created on : Apr 22, 2020, 12:43:11 PM
    Author     : Heca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete Department Page</title>
    </head>
    <body>
        <%@include file="../template/menu.jsp" %>
        <div style="color:red">
            <h1> ${message}</h1>
        </div>
    </body>
</html>
