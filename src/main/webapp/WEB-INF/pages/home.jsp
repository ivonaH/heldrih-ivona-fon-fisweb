<%-- 
    Document   : home
    Created on : Apr 21, 2020, 7:58:48 PM
    Author     : Heca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
    </head>
    <body>
        <%@include file="template/menu.jsp" %>

        
        <div class="container">
            <h1>This is home page.</h1>
            <div class="row">
                <div class="col">
                </div>
                <div class="col-6">
                    <div class="list-group">
                        <a href="/ivona/application/department/save" class="list-group-item list-group-item-action">Add department</a>
                        <a href="/ivona/application/department/all" class="list-group-item list-group-item-action">All departments</a>
                    </div>

                </div>
                <div class="col">
                </div>
            </div>
        </div>
    </body>
</html>
