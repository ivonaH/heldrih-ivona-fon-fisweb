<%-- 
    Document   : login
    Created on : Apr 21, 2020, 7:37:46 PM
    Author     : Heca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">

    </head>
    <body style="background-color:lightgray;">
        <div class="container" >

            <div class="row" style="margin-top: 80px;">
                <div class="col">
                </div>
                <div class="col-6">

                    <h1>Login</h1>
                    <div style="color:red">
                        ${message}
                    </div>
                    <form action="/ivona/application/login" method="POST">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="Enter email">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="name">Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter password">
                        </div>
                        <br>
                        <div>
                            <input type="submit" value="Login" id="Login"/>
                        </div>
                    </form>
                    <br>
                    <br>
                    <br>
                </div>
                <div class="col">
                </div>
            </div>
        </div>
    </body>
</html>
